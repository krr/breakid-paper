%The previous sections describe theoretical ideas and practical algorithms that have the potential to improve symmetry breaking for SAT solving.
This section describes how the improvements presented in the previous section combine with eachother and with standard symmetry breaking techniques in the symmetry breaking preprocessor \breakid.

%\subsection{A short history}
\newcommand\breakidone{\logicname{BreakID1}}
\newcommand\breakidtwoone{\logicname{BreakID2.1}}
\newcommand\breakidtwo{\logicname{BreakID2}}
\breakid has been around since 2013, when a preliminary version obtained the gold medal in the hard combinatorial sat+unsat track of 2013's SAT competition~\cite{url:SATcomp2013}. This early version incorporated all of \shatter's symmetry breaking techniques and used a primitive row interchangeability detection algorithm that enumerated symmetries to detect as many row swap symmetries as possible~\cite{cspsat/DevriendtBB14}.
%\breakidone was not easy to use, chaining multiple preprocessing executables and running \saucy as an external library.
%Nonetheless, \breakidone was unexpectedly performant, winning a gold medal on 2013's SAT competition in combination with the SAT-solver \glucose 2.1.
We developed \breakidtwo in 2015, using the ideas presented in this paper. 
%It incorporates \saucy in its source code and limits the CNF preprocessing workflow to only the \breakidtwo executable.
\breakidtwo entered the main track of 2015's SAT race in combination with \glucose 4.0, placing 10th, ahead of all other \glucose variants.
The experiments in the next section are run with a slightly updated version -- \breakidtwoone -- which has more usability features and reduced memory overhead. For the remainder of this paper, we use \breakid to refer to the particular implementation \breakidtwoone.
%
\breakid's source code is published online~\cite{url:breakid}.

\subsection{\breakid's high level algorithm}
Preprocessing a formula $\phi$ by symmetry breaking in \breakid starts with removing duplicate clauses and duplicate literals in clauses from $\phi$, as $\saucy$ cannot handle duplicate edges. Then, a call to \saucy constructs an initial generator set $P$ of the syntactical symmetry group of $\phi$.

Thirdly, \breakid detects row interchangeability subgroups $R_M$ of $Grp(P)$ by Algorithm \ref{alg:intch_detection}. The program incorporates the variables of the support of all $R_M$ in a global variable order $Ord$ such that the conjunction of $LL_{\pi_\rho^M}$ under $Ord$ for all subsequent row swaps $\pi_\rho^M$ forms a complete symmetry breaking formula for $R_M$.\footnote{In case two detected row interchangeability matrices overlap, it is not always possible to choose the order on the variables so that both are broken completely. In this case, one of the row interchangeability groups will only be broken partially.}
After adding the complete symmetry breaking formula of each $R_M$ to an initial set of symmetry breaking clauses $\psi$, we also remove all symmetries in $P$ that belong to some $R_M$, since these symmetries are broken completely already.
%\bart{Gelijkaardig argument als in sectie 5. Gewoon generatoren verwijderen is geen goede aanpak. Eigenlijk zou je een set generatoren van de quotientgroep moeten berekenen.}\bart{Quotientgroep was een slecht woord. Moet je hier geen stabilizer deelgroep nemen? VB de deelgroep die all behalve de eerste rij stabilissert? Dat is degene waar je in geinteresseerd bent, he.}
%\bart{Hmm... stabiliser is ook niet goed. Wel... Het is goed in de zin dat het correct is, maar alles is hier correct. Het doel is: gooi enkel dingen weg waarvan je zeker bent dat ze al volledig gebroken zijn. Dan is uw approach misschien toch niet slecht. Misschien gooi je een paar generatoren te weinig weg, maar die gaan je nooit schaden. Dus vergeet wat ik hierover heb gezegd!}
%\jo{Volledig akkoord, heb dezelfde redenering op een gegeven moment gemaakt, maar nooit neergeschreven.}

Next, using the pruned $P$, binary clauses for $Grp(P)$ are constructed by Algorithm \ref{alg:binary_clauses}, which simultaneously decides a set of variables to be smallest under $Ord$.\footnote{A small adaptation to Algorithm \ref{alg:binary_clauses} ensures \breakid only selects smallest variables that are not permuted by a previously detected row interchangeability group.}

Finally, $Ord$ is supplemented with missing variables until it is total, and limited lex-leader constraints $LL_\pi^{50}$ are constructed for each $\pi$ left in $P$.
These lex-leader constraints incorporate two extra refinements also used by \shatter; one for \emph{phase-shifted} variables and one for the \emph{largest variable in a symmetry cycle}~\cite{2002Aloul}.

Algorithm \ref{alg:breakid} gives pseudocode for \breakid's high-level routine described above.

\begin{algorithm}%[Interchangeability detection]%BART: this does not compile
\label{alg:breakid}
\caption{Symmetry breaking by \breakid}
\SetKwInOut{Input}{input}
\SetKwInOut{Output}{output}
\Input{$\phi$}
\Output{$\psi$}
  remove duplicate clauses from $\phi$ and duplicate literals from clauses in $\phi$\;
  run \textbf{\saucy} to detect a set of symmetry generators $P$\;
  initialize $\psi$ as the empty formula and $Ord$ as an empty sequence of ordered variables\;
	detect \textbf{row interchangeability} subgroups $R_M$\;
	\ForEach{row interchangeability $R_M$ subgroup of $Grp(P)$}{
		remove $P \cap R_M$ from $P$\;
		add complete symmetry breaking clauses for $R_M$ to $\psi$\;
		add $Supp(R_M)$ to the back of $Ord$ accordingly\;
	}
	add \textbf{binary clauses} for $Grp(P)$ to $\psi$, add corresponding variables to the front of $Ord$\;
	add missing variables to the middle of $Ord$\;
	\ForEach{$\pi \in P$}{
		add $LL_\pi^{50}$ to $\psi$, utilizing \textbf{\shatter's optimizations}\;
	}
  \Return $\psi$\;
\end{algorithm}
