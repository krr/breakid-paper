An important type of symmetry is \emph{row interchangeability}, which is present when a subset of variables can be structured as a two-dimensional matrix such that each permutation of the rows induces a symmetry.
This form of symmetry is common; often it stems from an interchangeable set of objects in the original problem domain, with each row of variables expressing certain properties of one particular object.
Examples are intercheangeability of pigeons or holes in a pigeonhole problem, interchangeability of nurses in a nurse scheduling problem, fleets of interchangeable trucks in a delivery system etc.
Exploiting this type of matrix symmetry with adjusted symmetry breaking techniques can significantly improve SAT performance~\cite{cspsat/DevriendtBB14,Lynce2007}.
In this section, we present a novel way of automatically dealing with row interchangeability in SAT.

\begin{example}[Row interchangeability in graph coloring]
\label{ex:graph}
Let $\phi$ be a CNF formula expressing the graph coloring constraint that two directly connected vertices cannot have the same color.
Let $\voc=\{x_{11},\ldots,x_{nm}\}$ be the set of variables, with intended interpretation that $x_{ij}$ holds iff vertex $j$ has color $i$.
Given the nature of the graph coloring problem, all colors are interchangeable, so each color permutation $\rho$ induces a symmetry $\pi_\rho$ of $\phi$.
More formally, the color interchangeability symmetry group consists of all symmetries
\[ 
\pi_\rho: \barvoc \rightarrow \barvoc: x_{ij} \mapsto x_{\rho(i)j}, \neg x_{ij} \mapsto \neg x_{\rho(i)j}
\]
If we structure $\voc$ as a matrix where $x_{ij}$ occurs on row $i$ and column $j$, then each permutation of rows corresponds to a permutation of colors, and hence a symmetry.
\end{example}

\begin{definition}[Row interchangeability in SAT~\cite{cspsat/DevriendtBB14}]
A \emph{variable matrix} $M$ is a bijection $M:\Mrow \times \Mcol \rightarrow \voc'$ from two sets of indices $\Mrow$ and $\Mcol$ to a set of variables $\voc' \subseteq \voc$. We refer to $M(r,c)$ as $x_{rc}$. The $r$'th \emph{row} of $M$ is the sequence of variables $[x_{r1},\ldots,x_{rm}]$, the $c$'th \emph{column} is the sequence $[x_{1c},\ldots,x_{nc}]$.
A formula $\phi$ exhibits \emph{row interchangeability} symmetry if there exists a variable matrix $M$ such that for each permutation $\rho: \Mrow \rightarrow \Mrow$
\[ 
\pi_\rho^M: \barvoc' \rightarrow \barvoc': x_{rc} \mapsto x_{\rho(r)c}, \neg x_{rc} \mapsto \neg x_{\rho(r)c} 
\]
is a symmetry of $\phi$. The row interchangeability symmetry group of a matrix $M$ is denoted as $R_M$.
\end{definition}

A useful property of row interchangeability is that it is broken completely by only a linearly sized symmetry breaking formula~\cite{row_column_sym_csp,dam/shlyakter2007}. %More specifically, given the right order on the literals, a set of generator symmetries exists for which the combined lex-leader constraint completely breaks the row interchangeability symmetry group.
\begin{theorem}[Complete symmetry breaking for row interchangeability~\cite{cspsat/DevriendtBB14}]
\label{thm:completeness}
Let $\phi$ be a formula and $R_M$ a row interchangeability symmetry group of $\phi$ with $\Mrow=\{1,\ldots,n\}$ and $\Mcol=\{1,\ldots,m\}$. If the total variable order $\lexvar$  on $\voc$ satisfies $x_{ij} \lexvar x_{i'j'}$ iff $i<i'$ or ($i=i'$ and $j \leq j'$), then the conjunction of lex-leader constraints for $\pi_{(k~k+1)}^M$ with $1 \leq k<n$ 
% \[\begin{array}{rl}
%   & \;\;\; 1 \leq i < n
% \end{array}\]
breaks $M_R$ completely.
\end{theorem}
%\bart{nieuw resultaat? Neen he: benadruk het mara door de cite in de stelling te plaatsen}
In words, Theorem \ref{thm:completeness} guarantees that for a natural ordering of the variable matrix, the lex-leader constraints for the swap of each two subsequent rows form a complete symmetry breaking formula for row interchangeability.
The condition that the order ``matches'' the variable matrix is important: the theorem no longer holds without it. 

If we are able to detect that a formula exhibits row interchangeability, we can break it completely by choosing the right order and posting the right lex-leader constraints.
In practice, symmetry detection tools for SAT only present us with a set of generators for the symmetry group, which contains no information on the \emph{structure} of this group.
The challenge is to derive row interchangeability subgroups from these generators. % such a generator set.

\subsection{Row interchangeability detection algorithm}
Given a set of generators $P$ for a symmetry group $\Pi$ of a formula $\phi$, the task at hand is to detect a variable matrix $M$ that represents a row interchangeability subgroup $R_M \subseteq \Pi$.
We present an algorithm that is sound, but incomplete in the sense that it does not guarantee that all row interchangeability subgroups present are detected.

The first step %in the interchangeability detection algorithm 
is to find an initial row interchangeable variable matrix $M$ consisting of three rows. This is done by selecting two swap symmetries $\pi_1$ and $\pi_2$ that represent two swaps of three rows.
More formally, suppose $\pi_1$ and $\pi_2$ are such that (with $r=Supp(\pi_1) \cap Supp(\pi_2)$) the following three conditions hold \begin{inparaenum}
\item $\pi_1=\pi_1^{-1}$ and $\pi_2=\pi_2^{-1}$, 
\item $r$, $\pi_1(r)$ and $\pi_2(r)$ are pairwise disjoint, and 
\item $Supp(\pi_1)= r \cup \pi_1(r)$ and $Supp(\pi_2) = r \cup \pi_2(r)$.                                                                                                             \end{inparaenum}
% d that .
In this case, $r$, $\pi_1(r)$ and $\pi_2(r)$ form three rows of a row interchangeable variable matrix, and $\pi_1$ and $\pi_2$ are swaps of those rows.
%We take $r$ to be the first row of the variable matrix.

If, after inspecting all pairs of swaps in $P$, no initial three-rowed matrix is found, the algorithm stops,
in which case we do not know whether a row interchangeability subgroup
exists.  However, our experiments indicate that for many problems,
an initial three-rowed matrix can be derived from a detected set of generator
symmetries.

The second step maximally extends the initial variable matrix $M$ with new rows. 
The idea is that for each symmetry $\pi\in P$ and each row $r$ of $M$, $\pi(r)$ is a candidate row to add to $M$.
This is the case if $\pi(r)$'s literals are disjoint from $M$'s literals and swapping $\pi(r)$ with $r$ is a syntactical symmetry of $\phi$.
% (which can be verified with a syntactical check on $\phi$).
%After all combinations of rows and generators are investigated, the algorithm queries \saucy for extra symmetry generators that might further extend the variable matrix.
%To push \saucy to give symmetries that lead to extended rows of $M$, all variables of $M$ except those on the first row are given a unique color.
%This way, \saucy can only detect symmetries that permute the first row, unencumbered by permutations over variables of the other rows (that might have nothing to do with the current matrix $M$).


\begin{algorithm}%[Interchangeability detection]%BART: this does not compile
\label{alg:intch_detection}
\caption{Row interchangeability detection}
\SetKwInOut{Input}{input}
\SetKwInOut{Output}{output}
\Input{$P$, $\phi$}
\Output{$M$}
  identify two swaps $\pi_1, \pi_2 \in P$ that induce an initial variable matrix $M$ with $3$ rows\;
  \Repeat{no extra rows are added to $M$}{
    \ForEach{permutation $\pi$ in $P$}{
      \ForEach{row $r$ in $M$}{
        \If{$\pi(r)$ is disjoint from $M$ and swapping $r$ and $\pi(r)$ is a symmetry of $\phi$}{
          add $\pi(r)$ as a new row to $M$\;
        }
      } 
    }
    %give any variable node from a variable in $M$ not on the first row a unique color in \saucy's graph\;
    %rerun \saucy to extend $P$ with new symmetry generators\;
  }
  \Return $M$\;
\end{algorithm}

%\bart{dit algoritme vereist dat je weet hoe saucy werkt... het spreekt over kleuren. Moet uitgelegd worden}
%\bart{Merk op: dit stopt mogelijk te vroeg: als er in de foreach geen nieuwe rijeen aan M toegevoegd worden, kan het toch nog zijn dat het SAUCY stuk P uitbreidt. In dat geval mag je nog niet uit de repeat springen}
%If the query to \saucy gives new generators, the algorithm goes back to extending the matrix. Otherwise, the algorithm terminates by returning the detected variable matrix, if any. 

Pseudocode is given in Algorithm \ref{alg:intch_detection}.
This algorithm terminates since both $P$ and the number of rows in any row interchangeability matrix are finite. 
The algorithm is sound: each time a row is added, it is interchangeable with at least one previously added row and hence, by induction, with all rows in $M$. 
If $k$ is the largest support size of a symmetry in $P$, then finding an initial row interchangeable matrix based on two row swap symmetries in $P$ takes $O(|P|^2k)$ time. With an optimized implementation that avoids duplicate combinations of generators and rows, extending the initial matrix with extra interchangeable rows has a complexity of $O(|P||\Mrow||\phi|k)$, with $\Mrow$ the set of row indices of $M$.
Algorithm \ref{alg:intch_detection} then has a complexity of $O(|P|^2k +|P||\Mrow||\phi|k)$.

As mentioned before, the algorithm is not complete: it might not be possible to construct an initial matrix, or even given an initial matrix, there is no guarantee to detect all possible row extensions, as only the set of generators instead of the whole symmetry group is used to calculate a new candidate row.

It is straightforward to extend Algorithm \ref{alg:intch_detection} to detect multiple row interchangeability subgroups. After detecting a first row interchangeability subgroup $R_M$, remove any generators from $P$ that also belong to $R_M$. This can be done by standard algebraic group membership tests, which are efficient for interchangeability groups~\cite{permutation_group_algorithms}. Then, repeat Algorithm \ref{alg:intch_detection} with the reduced set of generator symmetries until no more row interchangeability subgroups are detected.
%\jo{approximatief...}
% \bart{En eis dat uw variabelen niet overlappen? Of dat de orde die al opgelegd wordt door eerdere matrix gerespecteerd wordt? Of kies je de orde achteraf en zeg je dan ``sorry, sommige matrices worden toch niet volledig gebroken?''}
\begin{example}[Example \ref{ex:graph} continued.]%\bart{Beter zou zijn: example van begin van de section continued, dan moet je $x_{ij}, c_{ij}$ etc niet meer invoeren}
\label{ex:row_intch_detection}
Let $\varphi$ and the $x_{ij}$ be as in Example \ref{ex:graph}. Suppose we have five colors and three vertices. 
% Suppose we have a graph coloring problem with five colors and three vertices. 
Vertex 1 is connected to vertex 2 and 3; vertices 2 and 3 are not connected.
% Let $x_{ij}$ be a Boolean variable denoting whether vertex $j$ has color $i$.
%A CNF formula $\phi$ representing this problem is:
%\begin{align*}
%(\neg x_{i1} \vee \neg x_{i2}) & \;\;\; 1\leq i \leq 4\\
%(\neg x_{i1} \vee \neg x_{i3}) & \;\;\; 1\leq i \leq 4\\
%(x_{1j} \vee x_{2j} \vee x_{3j} \vee x_{4j}) & \;\;\; 1\leq j \leq 3\\
%(\neg x_{ij} \vee \neg x_{i'j}) & \;\;\; 1\leq j \leq 3, 1\leq i < i' \leq 4
%\end{align*}
%The first two sets of clauses state that vertex 1 and 2 and vertex 1 and 3 must have a different color.
%The third set states that each vertex must have a color, while the fourth states that one vertex can not have two different colors.
This problem has a symmetry group $\Pi$ induced by the interchangeability of the colors and by a swap on vertex 2 and 3.  A set of generators for $\Pi$ is $\{\pi_{(12)}, \pi_{(23)}, \pi_{(34)}, \pi_{(45)}, \nu_{23}\}$, where $ \pi_{(ij)}$ is the symmetry that swaps colors $i$ and $j$ (as in the previous example),
% \[c_{ij} = (x_{i1}~x_{j1})(x_{i2}~x_{j2})(x_{i3}~x_{j3})\]
and $\nu_{23}$ is the symmetry obtained by swapping vertices $2$ and $3$, i.e.,\footnote{We omit negative literals from the cycle
  notation, noting that a symmetry always commutes
  with negation.}
\begin{align*}
 \nu_{23} &= (x_{12}~x_{13})(x_{22}~x_{23})(x_{32}~x_{33})(x_{42}~x_{43})(x_{52}~x_{53}).
\end{align*}

% The interchangeability of the colors is generated by four row
% %swaps~\footnote{For succinctness' sake, we omit negative literals from the permutation cycle notation. The corresponding cycles can be derived via the commutation with negation property of symmetry in SAT.}:
% swaps:
% % 
% \begin{align*}
% c_{12} &= (x_{11}~x_{21})(x_{12}~x_{22})(x_{13}~x_{23})\\
% c_{23} &= (x_{21}~x_{31})(x_{22}~x_{32})(x_{23}~x_{33})\\
% c_{34} &= (x_{31}~x_{41})(x_{32}~x_{42})(x_{33}~x_{43})\\
% c_{45} &= (x_{41}~x_{51})(x_{42}~x_{52})(x_{43}~x_{53})
% \end{align*}
% And the swap on vertex 2 and 3 is generated by:
% \begin{align*}
%  \nu_{23} &= (x_{12}~x_{13})(x_{22}~x_{23})(x_{32}~x_{33})(x_{42}~x_{43})(x_{52}~x_{53}) \end{align*}

For these generators, it is obvious that the swaps $\pi_{(ij)}$ generate a row interchangeability symmetry group. However, a symmetry detection tool might return the alternative set of symmetry generators $P=\{\pi_{(12)},\pi_{(23)},\sigma_1 ,\sigma_2,\nu_{23}\}$ with %$\pi_1 = c_{12} ,\pi_2 = c_{23}, \pi_5=\nu_{23}$ and 
\begin{align*}
% % \pi_2 &= c_{23} = (x_{21}~x_{31})(x_{22}~x_{32})(x_{23}~x_{33})\\
\sigma_1 &= \pi_{(35)}\circ \pi_{(13)} = (x_{11}~x_{31}~x_{51})(x_{12}~x_{32}~x_{52})(x_{13}~x_{33}~x_{53})\\
\sigma_2 &= \pi_{(34)} \circ \nu_{23} = (x_{12}~x_{13})(x_{22}~x_{23})(x_{31}~x_{41})(x_{32}~x_{43})(x_{42}~x_{33})(x_{52}~x_{53}).
% \pi_5 &= \nu_{23} = (x_{12}~x_{13})(x_{22}~x_{23})(x_{32}~x_{33})(x_{42}~x_{43})(x_{52}~x_{53})
\end{align*}
% which generates the exact same symmetry group $\Pi$. 
The challenge is to detect the 	color interchangeability subgroup starting from $P$.

The first step  of Algorithm \ref{alg:intch_detection} searches for two swaps in $P$ that combine to a 3-rowed variable matrix. $\pi_{(12)}$ and $\pi_{(23)}$ fit the bill, resulting in a variable matrix $M$ with rows:
\begin{align*}
[x_{11},x_{12},x_{13}] \;\;\;
[x_{21},x_{22},x_{23}] \;\;\;
[x_{31},x_{32},x_{33}]
\end{align*}
Applying $\sigma_1$ on the third row results in:
\begin{align*}
[x_{51},x_{52},x_{53}]
\end{align*}
which after a syntactical check on $\phi$ is confirmed to be a new row to add to $M$.

Unfortunately, the missing row $[x_{41},x_{42},x_{42}]$ is not derivable by applying any generator in $P$ on rows in $M$, so the algorithm terminates.
\end{example}

The failure  of detecting the missing row in Example \ref{ex:row_intch_detection} stems from the fact that the generators $\sigma_1$ and $\sigma_2$ are obtained by complex combinations of symmetries in the interchangeability subgroup and the symmetry $\nu_{23}$. This inspires a small extension of Algorithm \ref{alg:intch_detection}. As soon as the algorithm reaches a fixpoint, we call the original symmetry detection tool to search for a set of generators of the subgroup that stabilizes all but one rows of the matrix $M$ found so far.
This results in ``simpler'' generators that do not permute the literals of the excluded rows. Tests on CNF instances show that this simple extension, although giving no theoretical guarantees, often manages to find new generators that, when applied on the current set of rows, construct new rows.
After detecting row stabilizing symmetries, Algorithm \ref{alg:intch_detection} resumes from line 2, aiming to extend the matrix further by applying the extended set of generators.
This process ends when even the new generators can no longer derive new rows.

% The only symmetry still left in the problem after fixing the variables
% $\{x_{21},x_{22},x_{23},x_{31},x_{32},x_{33},x_{51},x_{52},x_{53}\}$ is \[(x_{11}~x_{41})(x_{12}~x_{42})(x_{13}~x_{43})\] which has the missing row $[x_{41},x_{42},x_{42}]$ as image of the first row.


\begin{example}[Example \ref{ex:row_intch_detection} continued.]
The only symmetry of the problem that stabilizes the variables $\{x_{11},x_{12},x_{13},x_{21},x_{22},x_{23},x_{31},x_{32},x_{33}\}$ is $\pi_{(45)}$, which has the missing row $[x_{41},x_{42},x_{42}]$ as image of the fifth row.

The matrix, which now contains all variables, allows one to
  completely break the color interchangeability. The symmetry between
  vertices 2 and 3 is not expressed in the matrix, but can still be
  broken by a lex-leader constraint, as described in Section~\ref{sec:breakid}.
\end{example}



%\maurice{ En als je pech hebt, dan vind je eerst de matrix voor de
%  omwisselbaarheid van vertices 2 en 3?? Wat dan?}
%\jo{in dit geval niet, want je hebt 3 rijen nodig (complete symmetriebreking heeft pas nut vanaf matrixen met 3 rijen, anders ben je gewoon een swap aan het breken). In het algemeen kun je idd pech hebben, en een kleine matrix afleiden waar een grotere (volgens een andere dimensie) aanwezig was.}
